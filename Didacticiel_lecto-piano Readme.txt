Didacticiel_lecto-piano Readme.txt

Logiciel de lecture musicale et d'identification des notes sur la portée de piano.


Développé par Daniel Mancero 
© Daniel Mancero 2020

CICM - Université Paris 8

www.danielmancero.com
